//
//  BFInfoTVC.h
//  BalloonFiesta
//
//  Created by Matthew Ferguson on 1/16/16.
//

#import <UIKit/UIKit.h>
#import "InfoCell.h"
#import "InfoElement.h"
#import "InfoElementS.h"
#import "InfoDetailVC.h"

@interface BFInfoTVC : UITableViewController



@property (nonatomic, strong) InfoElement *stagingInfoItemDetails;

-(IBAction)prepareForUnwindInfoDetailToInfoTVC:(UIStoryboardSegue *)segue;

@end
