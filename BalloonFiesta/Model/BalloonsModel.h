//
//  BalloonsModel.h
//  BalloonFiesta
//
//  Created by Matthew Ferguson on 1/16/16.
//

#import <Foundation/Foundation.h>
#import "SingleBalloon.h"

@interface BalloonsModel : NSObject
{
    
    //****** Balloon Objects by Sorted by Name ***********
    //****************************************************
    
    //holds the index letters, first letter of name as a key,
    //      as balloon data (MutableArray of SingleBalloon).
    NSMutableDictionary *   balloonsByNamesDictionary;
    //holds the first letter as a key, of balloon names,
    //   with the value balloon data as the object - KVO.
    NSMutableDictionary *   balloonsByNameIndexesDictionary;
    //holds the first letter of balloon names from within each SingleBalloon object.
    //   Used to set the section header of the table view
    NSArray             *   balloonsByNameIndexArray;
    
    
    //****** Balloon Objects by Sorted by Number *********
    //****************************************************
    
    //holds the index of balloon number, number key,
    //      as balloon data (MutableArray of SingleBalloon).
    NSMutableDictionary *   balloonsByNumbersDictionary;
    NSMutableArray      *   balloonsSortedByNumber;
    NSMutableArray      *   staticBalloonsSortedByNumber;
    
    
    
    //****** Balloon Objects because of Search Output ****
    //****************************************************
    NSMutableDictionary *   balloonsSearchDictionary;
    NSMutableArray      *   balloonsSearchArray;
    NSMutableArray      *   balloonsSwapSearchArray;

}



@property (nonatomic,strong) NSMutableDictionary *   balloonsByNamesDictionary;
@property (nonatomic,strong) NSMutableDictionary *   balloonsByNameIndexesDictionary;
@property (nonatomic,strong) NSArray             *   balloonsByNameIndexArray;

@property (nonatomic,strong) NSMutableDictionary *   balloonsByNumbersDictionary;
@property (nonatomic,strong) NSMutableArray      *   balloonsSortedByNumber;
@property (nonatomic,strong) NSMutableArray      *   staticBalloonsSortedByNumber;

@property (nonatomic,strong) NSMutableDictionary *   balloonsSearchDictionary;
@property (nonatomic,strong) NSMutableArray      *   balloonsSearchArray;
@property (nonatomic,strong) NSMutableArray      *   balloonsSwapSearchArray;


+ (instancetype)sharedInstance;

- (void)presortBalloonsInitialLetterIndexes;
- (NSArray *) balloonsWithInitialLetter:(NSString*)aKey;


- (void)loadSearchBalloon:(SingleBalloon *)eachBalloon;
- (void)emptySearchBalloonModel;


- (void)swapToSearchModel;
- (void)swapToOriginalModel;

@end
