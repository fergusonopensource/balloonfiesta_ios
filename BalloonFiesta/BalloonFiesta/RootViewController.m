//
//  ViewController.m
//  BalloonFiesta
//
//  Created by Matthew Ferguson on 1/16/16.
//

#import "RootViewController.h"

@interface RootViewController ()

@end

@implementation RootViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [BalloonsModel sharedInstance];
    [InfoElementS sharedInstance];
}



- (void) viewDidAppear:(BOOL)animated
{
    [self performSegueWithIdentifier:@"Root_TableVC_Segue" sender:self];
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}



- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    
    // Make sure your segue name in storyboard is the same as this line
    if ([[segue identifier] isEqualToString:@"Root_TableVC_Segue"])
    {
        //placeholder
    }
}


@end
