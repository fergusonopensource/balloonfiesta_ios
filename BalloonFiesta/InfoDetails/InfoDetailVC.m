//
//  InfoDetailVC.m
//  BalloonFiesta
//
//  Created by Matthew Ferguson on 1/19/16.
//

#import "InfoDetailVC.h"

@interface InfoDetailVC ()

@end

@implementation InfoDetailVC

@synthesize myWebView;




- (void)viewDidLoad
{
    [super viewDidLoad];
}

-(void) viewWillAppear:(BOOL)animated
{
    self.myWebView.backgroundColor = [UIColor lightGrayColor];
    
    if(self.infoElement.scaleToFit)
    {
        self.myWebView.scalesPageToFit = NO;
    }
    else
    {
        self.myWebView.scalesPageToFit = YES;
    }
    
    
    self.myWebView.autoresizingMask = (UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight);
    self.myWebView.delegate = (id)self;
    
    NSString *pdfPath = [[NSBundle mainBundle] pathForResource:self.infoElement.filename ofType:@"png"];
    NSURL *pdfURL = [NSURL URLWithString:pdfPath];
    [self.myWebView loadRequest:[NSURLRequest requestWithURL:pdfURL]];
        
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
