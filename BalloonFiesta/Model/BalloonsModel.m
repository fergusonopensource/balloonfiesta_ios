//
//  BalloonsModel.m
//  BalloonFiesta
//
//  Created by Matthew Ferguson on 1/16/16.
//
//  Runtime Model Singleton Pattern
//   Reads PLIST files included in the bundle.
//


#import "BalloonsModel.h"

@implementation BalloonsModel


@synthesize   balloonsByNamesDictionary;
@synthesize   balloonsByNameIndexesDictionary;
@synthesize   balloonsByNameIndexArray;

@synthesize   balloonsByNumbersDictionary;
@synthesize   balloonsSortedByNumber;
@synthesize   staticBalloonsSortedByNumber;

@synthesize   balloonsSearchDictionary;
@synthesize   balloonsSearchArray;
@synthesize   balloonsSwapSearchArray;



+ (instancetype)sharedInstance
{
    static dispatch_once_t once;
    static id sharedInstance;
    
    dispatch_once(&once, ^{
        sharedInstance = [[self new] init];
    });
    
    return sharedInstance;
}


// setup the data collection
- init
{
    if (self = [super init])
    {
        [self setupBalloonsArray];
    }
    return self;
}



- (void)setupBalloonsArray
{
    
    NSDictionary * aBalloonObject;
    
    self.balloonsSearchDictionary = [NSMutableDictionary dictionary];
    self.balloonsSearchArray = [NSMutableArray array];
    self.staticBalloonsSortedByNumber = [NSMutableArray array];
    self.balloonsByNamesDictionary = [NSMutableDictionary dictionary];
    self.balloonsByNumbersDictionary = [NSMutableDictionary dictionary];
    
    // unique first characters (for the Name index table)
    self.balloonsByNameIndexesDictionary = [NSMutableDictionary dictionary];
    
    // create empty array entries in the states Dictionary or each physical state
    [balloonsByNumbersDictionary setObject:[NSMutableArray array] forKey:@"abifNumber"];

    // read the element data from the plist
    NSString *thePath = [[NSBundle mainBundle]
                         pathForResource:@"BalloonsData" ofType:@"plist"];
    NSArray *rawBalloonsArray = [[NSArray alloc] initWithContentsOfFile:thePath];
    
    // iterate over the values in the raw balloon dictionary
    for (aBalloonObject in rawBalloonsArray)
    {
        // create an balloon instance for each
        SingleBalloon *aBalloon = [[SingleBalloon alloc] initWithDictionary:aBalloonObject];
        
        // Balloon info in the by name dictionary with the name of the balloon as the key.
        [balloonsByNamesDictionary setObject:aBalloon forKey:aBalloon.balloonName];
        
        // add that element to the appropriate array in the physical state dictionary
        [[balloonsByNumbersDictionary objectForKey:aBalloon.aibfNumber] addObject:aBalloon];
        
        // get the Balloon's initial letter
        NSString *firstLetter = [aBalloon.balloonName substringToIndex:1];
        NSMutableArray *existingArray;
        
        // if an array already exists in the name index dictionary
        // simply add the element to it, otherwise create an array
        // and add it to the name index dictionary with the letter as the key
        if ((existingArray = [balloonsByNameIndexesDictionary valueForKey:firstLetter]))
        {
            [existingArray addObject:aBalloon];
        }
        else
        {
            NSMutableArray *tempArray = [NSMutableArray array];
            [tempArray addObject:aBalloon];
            [balloonsByNameIndexesDictionary setObject:tempArray forKey:firstLetter];
        }
    }
    
    [self presortBalloonsInitialLetterIndexes];
    self.balloonsSortedByNumber = [self presortElementsByNumber];
    [self.staticBalloonsSortedByNumber addObjectsFromArray:self.balloonsSortedByNumber];

}



// presort the name index arrays so the elements are in the correct order
- (void)presortBalloonsInitialLetterIndexes
{
    
    self.balloonsByNameIndexArray =
        [[balloonsByNameIndexesDictionary allKeys]
         sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    
    for (NSString *eachNameIndex in balloonsByNameIndexArray)
    {
        [self presortElementNamesForInitialLetter:eachNameIndex];
    }
    
    
}



//***************************
// sort the balloons by name and sort them for nameIndexesDictionary
- (void)presortElementNamesForInitialLetter:(NSString *)aKey {
    NSSortDescriptor *nameDescriptor = [[NSSortDescriptor alloc] initWithKey:@"balloonName"
                                        ascending:YES
                                        selector:@selector(localizedCaseInsensitiveCompare:)];
    
    NSArray *descriptors = [NSArray arrayWithObject:nameDescriptor];
    
    [[balloonsByNameIndexesDictionary objectForKey:aKey] sortUsingDescriptors:descriptors];
}




// presort the elementsSortedByNumber array
- (NSMutableArray *)presortElementsByNumber
{
    
    NSMutableArray * myLocalMutArray = [NSMutableArray array];
    
    NSSortDescriptor *nameDescriptor = [[NSSortDescriptor alloc] initWithKey:@"aibfNumber"
                                        ascending:YES  selector:@selector(compare:)] ;
    
    NSArray *descriptors = [NSArray arrayWithObject:nameDescriptor];
    
    NSArray *sortedElements =
        [[balloonsByNamesDictionary allValues] sortedArrayUsingDescriptors:descriptors];
    
    [myLocalMutArray addObjectsFromArray:sortedElements];
    return myLocalMutArray;
}


// return an array of elements for an initial letter (ie A, B, C, ...)
- (NSArray *) balloonsWithInitialLetter:(NSString*)aKey
{
    return [balloonsByNameIndexesDictionary objectForKey:aKey];
}


// presort the elementsSortedByNumber array
- (void)swapToSearchModel
{
    [self.balloonsSortedByNumber  removeAllObjects];
    [self.balloonsSortedByNumber addObjectsFromArray:self.balloonsSearchArray];
}


// presort the elementsSortedByNumber array
- (void)swapToOriginalModel
{
    
    [self.balloonsSortedByNumber removeAllObjects];
    [self.balloonsSortedByNumber  addObjectsFromArray:self.staticBalloonsSortedByNumber ];
    [self.balloonsSearchArray removeAllObjects];

}


-(void)emptySearchBalloonModel
{
    [self.balloonsSearchArray removeAllObjects];
}


-(void)loadSearchBalloon:(SingleBalloon *)aBalloon
{
    [self.balloonsSearchArray addObject:aBalloon];
}


@end
