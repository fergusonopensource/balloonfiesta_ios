//
//  InfoElementS.m
//  "BalloonFiesta"
//
//  Created by MobileSandbox1 on 8/7/10.
//
//  Description:  'S' in the name InfoElementS.m/.h makes this a utility object
//                  to hold and manage data structures of balloons. 
//


#import "InfoElementS.h"
#import "InfoElement.h"

@interface InfoElementS (mymethods)
- (void) setupInfoArray;
@end

@implementation InfoElementS


@synthesize infoDictionary;
@synthesize infoIndexArray;

@synthesize elementsSortedByNumber;



+ (instancetype)sharedInstance
{
    static dispatch_once_t once;
    static id sharedInstance;
    
    dispatch_once(&once, ^{
        sharedInstance = [[self new] init];
    });
    
    return sharedInstance;
}



-(id) init {
    if (self = [super init]){
		[self setupInfoArray];
	}
	return self;
}




- (void)setupInfoArray 
{
    
	NSDictionary *eachElement;
    
	self.infoDictionary = [NSMutableDictionary dictionary];
	
	NSString *thePath = [[NSBundle mainBundle]  pathForResource:@"InfoData" ofType:@"plist"];
	NSArray *rawBalloonsArray = [[NSArray alloc] initWithContentsOfFile:thePath];

	// iterate over the values in the raw balloon dictionary
	for (eachElement in rawBalloonsArray)
	{
		// create an atomic element instance for each
		InfoElement *anInfoPdfPathElement =
            [[InfoElement alloc] initWithDictionary:eachElement];
		
		[infoDictionary setObject:anInfoPdfPathElement forKey:anInfoPdfPathElement.tableCellListing ];

	}
	self.elementsSortedByNumber = [self presortElementsByNumber];
}



- (NSArray *)presortElementsByNumber 
{

    NSSortDescriptor *nameDescriptor = [[NSSortDescriptor alloc] initWithKey:@"targetIndex"
                                        ascending:YES selector:@selector(compare:)];
	
	NSArray *descriptors = [NSArray arrayWithObject:nameDescriptor];
	NSArray *sortedElements = [[infoDictionary allValues]
                sortedArrayUsingDescriptors:descriptors];
    
	return sortedElements;	
}

@end
