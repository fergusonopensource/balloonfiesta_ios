//
//  ByNameCell.h
//  BalloonFiesta
//
//  Created by Matthew Ferguson on 1/16/16.
//

#import <UIKit/UIKit.h>
#import "SingleBalloon.h"

@interface ByNameCell : UITableViewCell

@property (nonatomic,retain) SingleBalloon * singleBalloon;
@property (nonatomic, weak) IBOutlet UILabel * ByNameTextLabel;
@property (nonatomic, weak) IBOutlet UIImageView * ByNameThumbnail;

- (void)setBalloon:(SingleBalloon *)anElement;

@end
