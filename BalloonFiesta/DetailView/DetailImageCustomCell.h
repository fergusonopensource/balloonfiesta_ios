//
//  DetailImageCustomCell.h
//  BalloonFiesta
//
//  Created by Matthew Ferguson on 1/18/16.
//  Copyright © 2016 MobileSandbox. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailImageCustomCell : UITableViewCell


@property (nonatomic, weak) IBOutlet UIImageView * staticBackGround;
@end
