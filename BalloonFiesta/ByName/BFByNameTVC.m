//
//  BFByNameTVC.m
//  BalloonFiesta
//
//  Created by Matthew Ferguson on 1/16/16.
//

#import "BFByNameTVC.h"

@interface BFByNameTVC ()

@end

@implementation BFByNameTVC


@synthesize   stagingBalloonDetails, myTableView;



- (void)viewDidLoad
{
    
    [super viewDidLoad];

    UIFont *font = [UIFont fontWithName:@"HelveticaNeue-Light" size:22.0f];
    NSMutableDictionary *navBarTextAttributes =
    [NSMutableDictionary dictionaryWithCapacity:1];
    [navBarTextAttributes setObject:font forKey:NSFontAttributeName];
    self.navigationController.navigationBar.titleTextAttributes = navBarTextAttributes;
    
}




-(void) viewWillAppear:(BOOL)animated
{
    
    [BalloonsModel sharedInstance];//load an instance of singleton model to make sure data is available
    self.navigationController.navigationBar.topItem.title =  @"By Balloon Fiesta® Name";

}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


#pragma mark - Table view data source

- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView
{
    // returns the array of section titles. There is one entry for each unique character that an element begins with
    // [A,B,C,D,E,F,G,H,I,K,L,M,N,O,P,R,S,T,U,V,X,Y,Z]
    return [[BalloonsModel sharedInstance] balloonsByNameIndexArray];
}



- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [[[BalloonsModel sharedInstance] balloonsByNameIndexArray] count];
}



- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 28;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    // the section represents the initial letter of the element
    // return that letter
    NSString *initialLetter = [[[BalloonsModel sharedInstance] balloonsByNameIndexArray] objectAtIndex:section];
    
    // get the array of elements that begin with that letter
    NSArray * balloonWithInitialLetter = [[BalloonsModel sharedInstance] balloonsWithInitialLetter:initialLetter];
   
    return [balloonWithInitialLetter count];
}




- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{

    ByNameCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ByNameCustomCell_ID" forIndexPath:indexPath];
    if (cell == nil)
    {
        cell = [[ByNameCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"ByNameCustomCell_ID"];
    }
    
    NSString * name = [[[BalloonsModel sharedInstance] balloonsByNameIndexArray] objectAtIndex:indexPath.section] ;
    cell.singleBalloon = [[[BalloonsModel sharedInstance] balloonsWithInitialLetter:name] objectAtIndex:indexPath.row];
    [cell setBalloon:[[[BalloonsModel sharedInstance] balloonsWithInitialLetter:name] objectAtIndex:indexPath.row]];

    return cell;
}




- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{

    // this table has multiple sections. One for each unique character that an element begins with
    // [A,B,C,D,E,F,G,H,I,K,L,M,N,O,P,R,S,T,U,V,X,Y,Z]
    // return the letter that represents the requested section
    // this is actually a delegate method, but we forward the request to the datasource in the view controller
    
    return [[[BalloonsModel sharedInstance] balloonsByNameIndexArray] objectAtIndex:section];
}




-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString * name = [[[BalloonsModel sharedInstance] balloonsByNameIndexArray] objectAtIndex:indexPath.section];
    NSArray * initLetterArray = [[BalloonsModel sharedInstance] balloonsWithInitialLetter:name];
    self.stagingBalloonDetails = [initLetterArray objectAtIndex:indexPath.row];
    
    [self performSegueWithIdentifier:@"BFDetail_Segue" sender:self];
    
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/



#pragma mark - Navigation

-(IBAction)prepareForUnwind:(UIStoryboardSegue *)segue
{

}


- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    BFDetailVC* userViewController = [segue destinationViewController];
    userViewController.singleBalloon = self.stagingBalloonDetails;
}


@end