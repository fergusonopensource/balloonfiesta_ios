//
//  BFDetailVC.h
//  BalloonFiesta
//
//  Created by Matthew Ferguson on 1/18/16.
//

#import <UIKit/UIKit.h>
#import "DetailImageCustomCell.h"
#import "DetailTextCustomCell.h"
#import "SingleBalloon.h"

@interface BFDetailVC : UIViewController 


@property (nonatomic, strong) SingleBalloon * singleBalloon;

@property (nonatomic, weak) IBOutlet UIScrollView * backgroundScrollView;

@property (nonatomic, strong) IBOutlet UIImageView * backgroundView;
@property (nonatomic, strong) IBOutlet UIImageView * balloonView;

@property (nonatomic, weak) IBOutlet UILabel * balloonNameLabel;
@property (nonatomic, weak) IBOutlet UILabel * balloonBannerNumberLabel;
@property (nonatomic, weak) IBOutlet UILabel * balloonLaunchLocationLabel;
@property (nonatomic, weak) IBOutlet UILabel * balloonPilotNameLabel;
@property (nonatomic, weak) IBOutlet UILabel * balloonCityLabel;
@property (nonatomic, weak) IBOutlet UILabel * balloonHomeCountryLabel;
@property (nonatomic, weak) IBOutlet UILabel * balloonManufacturerYrLabel;

@property (nonatomic, weak) IBOutlet UIButton * buttonDone;


-(IBAction) DoneAction:(id)sender;
//-(IBAction) unwindToContainerVC:(UIStoryboardSegue *)segue;
//-(IBAction)prepareForUnwind:(UIStoryboardSegue *)segue;

@end
