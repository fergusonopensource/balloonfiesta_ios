//
//  InfoCell.m
//  BalloonFiesta
//
//  Created by Matthew Ferguson on 1/16/16.
//

#import "InfoCell.h"

@implementation InfoCell


@synthesize infoTextLabel;


- (void)awakeFromNib
{

}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}


- (void)setInfo:(InfoElement *)anInfoNode
{
    self.infoTextLabel.text = anInfoNode.tableCellListing;
    self.singleInfoNode = anInfoNode;
}


@end
