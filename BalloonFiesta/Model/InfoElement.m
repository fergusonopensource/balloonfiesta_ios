//
//  InfoElement.m
//  AIBF2010
//
//  Created by MobileSandbox1 on 8/7/10.
//

#import "InfoElement.h"

@implementation InfoElement


@synthesize tableCellListing;
@synthesize filename;
@synthesize targetIndex;
@synthesize isALink;
@synthesize isTheTracker;
@synthesize scaleToFit;
@synthesize isTheClearance;



- (id)initWithDictionary:(NSDictionary *)aDictionary 
{
	if ([self init]) 
	{
		self.tableCellListing = [aDictionary valueForKey:@"tableViewName"];
		self.filename = [aDictionary valueForKey:@"filename"];
		self.targetIndex = [aDictionary valueForKey:@"targetIndex"];
		self.isALink = [[aDictionary valueForKey:@"isALink"] boolValue];
		self.isTheTracker = [[aDictionary valueForKey:@"isTheTracker"] boolValue];
		self.scaleToFit  = [[aDictionary valueForKey:@"scaleToFit"] boolValue];
		self.isTheClearance = [[aDictionary valueForKey:@"isTheClearance"] boolValue];

	}
	return self;
}



- (UIImage *)stateImageForAtomicElementTileView {
	
	return [UIImage imageNamed:[NSString stringWithFormat:@"balloon_37_final.png"]];
}


- (UIImage *)stateImageForAtomicElementView {

	return [UIImage imageNamed:[NSString stringWithFormat:@"flip_screen.png"]];
}

@end
