//
//  BFByNameTVC.h
//  BalloonFiesta
//
//  Created by Matthew Ferguson on 1/16/16
//

#import <UIKit/UIKit.h>
#import "ByNameCell.h"
#import "BalloonsModel.h"
#import "BFDetailVC.h"


@interface BFByNameTVC : UIViewController


@property (nonatomic, strong) SingleBalloon *stagingBalloonDetails;
@property (nonatomic, strong) IBOutlet UITableView * myTableView;


-(IBAction)prepareForUnwind:(UIStoryboardSegue *)segue;

@end
