//
//  SingleBalloon.h
//  AIBF2013
//
//  Created by MobileSandbox1 on 7/17/10.
//

@import UIKit;
@import Foundation;

@interface SingleBalloon : NSObject
{
	NSNumber *aibfNumber;
	NSString *aibfNumberString;
	NSString *pilot;
	NSString *owner;
	NSString *balloonName;
	NSString *cityNstate;
	NSString *country;
	NSString * myPicture;
	NSString * yearsBallooning;
	NSString * launchLocation;
	NSString * manufactured;
	
}


- (id)initWithDictionary:(NSDictionary *)aDictionary;


@property (nonatomic, retain) NSNumber * aibfNumber;
@property (nonatomic, retain) NSString * aibfNumberString;
@property (nonatomic, retain) NSString * pilot;
@property (nonatomic, retain) NSString * owner;
@property (nonatomic, retain) NSString * balloonName;
@property (nonatomic, retain) NSString * cityNstate;
@property (nonatomic, retain) NSString * country;
@property (nonatomic, retain) NSString * myPicture;
@property (nonatomic, retain) NSString * yearsBallooning;
@property (nonatomic, retain) NSString * launchLocation;
@property (nonatomic, retain) NSString * manufactured;



@property (readonly) UIImage * stateImageForAtomicElementTileView;
@property (readonly) UIImage * stateImageForAtomicElementView;



@end
