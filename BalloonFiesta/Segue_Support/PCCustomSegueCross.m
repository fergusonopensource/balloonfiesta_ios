//
//  PCCustomSegueCross.m
//  PCLoginRnD
//
//  Created by Matthew Ferguson on 9/12/15.
//  Copyright © 2015 Matthew Ferguson. All rights reserved.
//

#import "PCCustomSegueCross.h"

@implementation PCCustomSegueCross

- (void) perform
{
    
    UIViewController *src = (UIViewController *) self.sourceViewController;
    UIViewController *dst = (UIViewController *) self.destinationViewController;
    [UIView transitionWithView:src.navigationController.view
                      duration:0.4
                       options: UIViewAnimationOptionTransitionCrossDissolve
                    animations:^{
                        
                        [src.navigationController pushViewController:dst animated:NO];
                        
                    }
                    completion:NULL];
    
}




@end
