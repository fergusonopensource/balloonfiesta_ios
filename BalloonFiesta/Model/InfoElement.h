//
//  InfoElement.h
//  AIBF2010
//
//  Created by MobileSandbox1 on 8/7/10.
//

#import <Foundation/Foundation.h>
@import UIKit;


@interface InfoElement : NSObject 
{
	NSString * filename;
	NSString * tableCellListing;
	NSNumber* targetIndex;
	BOOL isALink;
	BOOL isTheTracker;
	BOOL scaleToFit;
	BOOL isTheClearance;
}


- (id) initWithDictionary:(NSDictionary *)aDictionary;
- (UIImage*)  stateImageForAtomicElementView;
- (UIImage *) stateImageForAtomicElementTileView;


@property (nonatomic, strong) NSNumber * targetIndex;
@property (nonatomic, strong) NSString * tableCellListing;
@property (nonatomic, strong) NSString * filename;

@property (assign) BOOL isALink;
@property (assign) BOOL isTheTracker;
@property (assign) BOOL scaleToFit;
@property (assign) BOOL isTheClearance;


@end
