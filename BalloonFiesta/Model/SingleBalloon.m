//
//  BalloonElement.m
//  AIBF2010
//
//  Created by MobileSandbox1 on 7/17/10.
//

#import "SingleBalloon.h"


@implementation SingleBalloon



@synthesize aibfNumber;
@synthesize aibfNumberString;
@synthesize pilot;
@synthesize owner;
@synthesize balloonName;
@synthesize cityNstate;
@synthesize country;
@synthesize myPicture;
@synthesize yearsBallooning;
@synthesize launchLocation;
@synthesize manufactured;


- (id)initWithDictionary:(NSDictionary *)aDictionary 
{
	
	if ([self init]) 
	{
		self.aibfNumber = [aDictionary valueForKey:@"aibfNumber"];
		self.aibfNumberString = [aDictionary valueForKey:@"aibfNumberString"];
		self.pilot = [aDictionary valueForKey:@"pilot"];
		self.owner = [aDictionary valueForKey:@"owner"];
        self.balloonName = [aDictionary valueForKey:@"balloonName"] ;
		self.cityNstate = [aDictionary valueForKey:@"cityNstate"];
		self.country = [aDictionary valueForKey:@"country"];
		self.myPicture = [aDictionary valueForKey:@"picture"];
		self.yearsBallooning = [aDictionary valueForKey:@"yearsBallooning"];
		self.launchLocation = [aDictionary valueForKey:@"launchLocation"];
		self.manufactured = [aDictionary valueForKey:@"manufactured"];
	}
	
	return self;
}
 
 
- (UIImage *)stateImageForAtomicElementTileView 
{
	
	return [UIImage imageNamed:[NSString stringWithFormat:@"balloon_37_final.png"]];
}


- (UIImage *)stateImageForAtomicElementView 
{

	return [UIImage imageNamed:[NSString stringWithFormat:@"flip_screen.png"]];

}

#if(0)
- (UIImage *)flipperImageForAtomicElementNavigationItem 
{
	
	// return a 30 x 30 image that is a reduced version
	// of the AtomicElementTileView content
	// this is used to display the flipper button in the navigation bar
	CGSize itemSize=CGSizeMake(30.0,30.0);
	UIGraphicsBeginImageContext(itemSize);
	
	
	UIImage *backgroundImage = [UIImage imageNamed:[NSString stringWithFormat:@"balloon_30_final.png"]];
    
	CGRect elementSymbolRectangle = CGRectMake(0,0, itemSize.width, itemSize.height);
	
    [backgroundImage drawInRect:elementSymbolRectangle];
	// draw the element number
	UIFont *font = [UIFont boldSystemFontOfSize:8];
	CGPoint point = CGPointMake(2,1);
	
	// draw the element symbol
	font = [UIFont boldSystemFontOfSize:13];
	CGSize stringSize = [self.aibfNumberString sizeWithFont:font];
	point = CGPointMake((elementSymbolRectangle.size.width-stringSize.width)/2,10);
	
    
    
    /*
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont fontWithName:@"HelveticaNeue" size:13]};
    // NSString class method: boundingRectWithSize:options:attributes:context is
    // available only on ios7.0 sdk.
    CGRect rect = [textToMeasure boundingRectWithSize:CGSizeMake(width, CGFLOAT_MAX)
                                              options:NSStringDrawingUsesLineFragmentOrigin
                                           attributes:attributes
                                              context:nil];
     */
	
    
	UIImage *theImage=UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	
    return theImage;
}
#endif

@end
