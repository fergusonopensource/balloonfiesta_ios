//
//  ByNumberCell.h
//  BalloonFiesta
//
//  Created by Matthew Ferguson on 1/16/16.
//

#import <UIKit/UIKit.h>
#import "SingleBalloon.h"

@interface ByNumberCell : UITableViewCell

@property (nonatomic,retain)            SingleBalloon * singleBalloon;

@property (nonatomic, weak) IBOutlet    UILabel * ByNumberTextLabel;
@property (nonatomic, weak) IBOutlet    UILabel * ByNumberNameTextLabel;
@property (nonatomic, weak) IBOutlet    UIImageView * ByNumberThumbnail;


- (void)setBalloon:(SingleBalloon *)anElement;

@end
