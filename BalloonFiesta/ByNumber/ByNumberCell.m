//
//  ByNumberCell.m
//  BalloonFiesta
//
//  Created by Matthew Ferguson on 1/16/16.
//

#import "ByNumberCell.h"

@implementation ByNumberCell

@synthesize  ByNumberTextLabel, ByNumberNameTextLabel, ByNumberThumbnail;


- (void)awakeFromNib {
    
    self.ByNumberNameTextLabel.text = [[NSString alloc]init];
    self.ByNumberTextLabel.text = [[NSString alloc]init];

}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}


// The cell node setter
// We implement this because the table cell values need
// to be updated when this property changes, and this allows
// for the changes to be encapsulated
- (void)setBalloon:(SingleBalloon *)anElement
{
    
    self.ByNumberTextLabel.text = anElement.aibfNumberString;
    self.ByNumberNameTextLabel.text = anElement.balloonName;
    
    UIImage * tempImage;
    tempImage = [UIImage imageNamed:anElement.myPicture];
    if(tempImage)
    {
        // good to go.
    }
    else
    {
        tempImage = [UIImage imageNamed:@"not_available.png"];
    }
    
    self.ByNumberThumbnail.image = tempImage;
}






@end
