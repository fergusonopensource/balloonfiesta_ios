//
//  InfoCell.h
//  BalloonFiesta
//
//  Created by Matthew Ferguson on 1/16/16.
//

#import <UIKit/UIKit.h>
#import "InfoElementS.h"
#import "InfoElement.h"

@interface InfoCell : UITableViewCell


@property (nonatomic, weak) IBOutlet UILabel * infoTextLabel;
@property (nonatomic, weak)          InfoElement * singleInfoNode;

- (void)setInfo:(InfoElement *)anInfoNode;

@end
