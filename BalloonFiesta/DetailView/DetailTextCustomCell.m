//
//  DetailTextCustomCell.m
//  BalloonFiesta
//
//  Created by Matthew Ferguson on 1/18/16.
//  Copyright © 2016 MobileSandbox. All rights reserved.
//

#import "DetailTextCustomCell.h"

@implementation DetailTextCustomCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
