//
//  BFByNumberTVC.m
//  BalloonFiesta
//
//  Created by Matthew Ferguson on 1/16/16.
//

#import "BFByNumberTVC.h"

@interface BFByNumberTVC ()
@end

@implementation BFByNumberTVC


@synthesize stagingBalloonDetails, myTableView, /*myTableDataSource,*/ sBar;

- (void)viewDidLoad
{

    [super viewDidLoad];
    
    //setting the nav bar font.
    UIFont *font = [UIFont fontWithName:@"HelveticaNeue-Light" size:18.0f];
    NSMutableDictionary *navBarTextAttributes = [NSMutableDictionary dictionaryWithCapacity:1];
    [navBarTextAttributes setObject:font forKey:NSFontAttributeName];
    self.navigationController.navigationBar.titleTextAttributes = navBarTextAttributes;
    
}




-(void) viewWillAppear:(BOOL)animated
{
    
    [BalloonsModel sharedInstance];// data source. Make sure it is loaded and available
    self.navigationController.navigationBar.topItem.title = @"By Balloon Fiesta® Number";
    self.sBar.delegate = self;
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSMutableArray * balloonsSortedbyNumber = [[BalloonsModel sharedInstance] balloonsSortedByNumber];
    return [balloonsSortedbyNumber count];
}




- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    ByNumberCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ByNumberCustomCell_ID" forIndexPath:indexPath];
    if (cell == nil){
        cell = [[ByNumberCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"ByNumberCustomCell_ID"];
    }
    [cell setBalloon:[[[BalloonsModel sharedInstance] balloonsSortedByNumber] objectAtIndex:indexPath.row]];
    
    return cell;
}



-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    self.stagingBalloonDetails = [[[BalloonsModel sharedInstance] balloonsSortedByNumber] objectAtIndex:indexPath.row];
    
    [self performSegueWithIdentifier:@"BFNumber_Detail_Segue" sender:self];
    
}




/*
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    // this table has multiple sections. One for each unique character that an element begins with
    // [A,B,C,D,E,F,G,H,I,K,L,M,N,O,P,R,S,T,U,V,X,Y,Z]
    // return the letter that represents the requested section
    // this is actually a delegate method, but we forward the request to the datasource in the view controller
    
    return [[[BalloonsModel sharedInstance] balloonsByNameIndexArray] objectAtIndex:section];
}
*/


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/



#pragma mark UISearchBarDelegate

// Delegate called when user enters/deletes text. Or the Cancel X is hit.
- (void) searchBar:(UISearchBar *)theSearchBar textDidChange:(NSString *)searchText {
    
    if ([searchText length] == 0) {
        sBar.showsCancelButton = NO;
        [[BalloonsModel sharedInstance] swapToOriginalModel];
        [self.myTableView reloadData];
    }
    
}


- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    
    [searchBar becomeFirstResponder];
    sBar.showsCancelButton = YES;
    [[BalloonsModel sharedInstance] swapToSearchModel];
    [self.myTableView reloadData];
    self.navigationController.navigationBar.topItem.title = @"Search balloon name, city, pilot?";

}




- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
{
    
    sBar.showsCancelButton = NO;
    [[BalloonsModel sharedInstance] swapToOriginalModel];
    [self.myTableView reloadData];
    
}


- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    
    @try{
        [[BalloonsModel sharedInstance] swapToOriginalModel];
        [self.myTableView reloadData];
    }
    @catch(NSException *e){
        NSLog(@"Threw an error  %@", e);
    }
    
    
    [searchBar resignFirstResponder];
    sBar.text = @"";
    self.navigationController.navigationBar.topItem.title =  @"By Balloon Fiesta® Number";
    
}




// called when Search button pressed
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{

    [searchBar resignFirstResponder];
    
    NSArray * localNameIndexesDictionary = [[[BalloonsModel sharedInstance] staticBalloonsSortedByNumber]copy];
    SingleBalloon * eachBalloon;
    BOOL found = NO;
    
    for( eachBalloon in localNameIndexesDictionary )
    {
        found = NO;
        
        if([eachBalloon.balloonName rangeOfString: searchBar.text options: NSCaseInsensitiveSearch].location != NSNotFound)
        { found = YES;}
        if([eachBalloon.owner rangeOfString: searchBar.text options: NSCaseInsensitiveSearch].location != NSNotFound)
        { found = YES; }
        if([eachBalloon.aibfNumberString rangeOfString: searchBar.text options: NSCaseInsensitiveSearch].location != NSNotFound)
        { found = YES; }
        if([eachBalloon.launchLocation rangeOfString: searchBar.text options: NSCaseInsensitiveSearch].location != NSNotFound)
        { found = YES; }
        if([eachBalloon.country rangeOfString: searchBar.text options: NSCaseInsensitiveSearch].location != NSNotFound)
        { found = YES; }
        if([eachBalloon.cityNstate rangeOfString: searchBar.text options: NSCaseInsensitiveSearch].location != NSNotFound)
        { found = YES; }
        
        if(found)
        {
            [[BalloonsModel sharedInstance] loadSearchBalloon:eachBalloon];
        }
        
    }
    
    [[BalloonsModel sharedInstance] swapToSearchModel];
    [self.myTableView reloadData];
    
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a
//      little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    BFDetailVC* userViewController = [segue destinationViewController];
    userViewController.singleBalloon = self.stagingBalloonDetails;
    self.navigationItem.title = @"Return";
}


-(IBAction)prepareForUnwind:(UIStoryboardSegue *)segue{
}

@end
