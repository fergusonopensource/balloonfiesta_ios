//
//  InfoElementS.h
//  AIBF2010
//
//  Created by MobileSandbox1 on 8/7/10.
//

#import <Foundation/Foundation.h>

@interface InfoElementS : NSObject 
{	
	NSMutableDictionary *infoDictionary;
	NSArray *infoIndexArray;
	NSArray *elementsSortedByNumber;
}

@property (nonatomic,retain) NSMutableDictionary * infoDictionary;
@property (nonatomic,retain) NSArray * infoIndexArray;
@property (nonatomic,retain) NSArray * elementsSortedByNumber;


- (NSArray *) presortElementsByNumber ;
+ (instancetype) sharedInstance;

- (void)setupInfoArray; 

@end
