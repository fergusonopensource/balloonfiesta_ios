//
//  BFByNumberTVC.h
//  BalloonFiesta
//
//  Created by Matthew Ferguson on 1/16/16.
//


#import <UIKit/UIKit.h>
#import "ByNumberCell.h"
#import "BFDetailVC.h"
#import "BalloonsModel.h"

@interface BFByNumberTVC : UIViewController <UISearchBarDelegate>

@property (nonatomic, strong) IBOutlet  UITableView   * myTableView;
@property (nonatomic, strong) IBOutlet  UISearchBar   * sBar;//search bar
@property (nonatomic, strong)           SingleBalloon * stagingBalloonDetails;


@end
