//
//  InfoDetailVC.h
//  BalloonFiesta
//
//  Created by Matthew Ferguson on 1/19/16.
//

#import <UIKit/UIKit.h>
#import "InfoElement.h"

@interface InfoDetailVC : UIViewController

@property (nonatomic, weak) IBOutlet UIWebView * myWebView;
@property (nonatomic, weak)          InfoElement * infoElement;


@end
