//
//  BFInfoTVC.m
//  BalloonFiesta
//
//  Created by Matthew Ferguson on 1/16/16.
//

#import "BFInfoTVC.h"

@interface BFInfoTVC ()

@end

@implementation BFInfoTVC


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIFont *font = [UIFont fontWithName:@"HelveticaNeue-Light" size:22.0f];
    NSMutableDictionary *navBarTextAttributes =
    [NSMutableDictionary dictionaryWithCapacity:1];
    [navBarTextAttributes setObject:font forKey:NSFontAttributeName];
    self.navigationController.navigationBar.titleTextAttributes = navBarTextAttributes;

    
    
    self.clearsSelectionOnViewWillAppear = YES;
}


-(void) viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBar.topItem.title = @"Balloon Fiesta® Information";

}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}



#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return ([[[InfoElementS sharedInstance] elementsSortedByNumber] count]);
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    InfoCell *cell = [tableView dequeueReusableCellWithIdentifier:@"InfoCustomCell" forIndexPath:indexPath];
    [cell setInfo:[[[InfoElementS sharedInstance] elementsSortedByNumber] objectAtIndex:indexPath.row]];

    return cell;
}



-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    self.stagingInfoItemDetails = [[[InfoElementS sharedInstance]
                                         elementsSortedByNumber] objectAtIndex:indexPath.row];
    
    [self performSegueWithIdentifier:@"InfoTable_InfoDetail_Segue" sender:self];
    
}





/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    InfoDetailVC* userViewController = [segue destinationViewController];
    userViewController.infoElement = self.stagingInfoItemDetails;
}


-(IBAction)prepareForUnwindInfoDetailToInfoTVC:(UIStoryboardSegue *)segue
{
    
}

@end
