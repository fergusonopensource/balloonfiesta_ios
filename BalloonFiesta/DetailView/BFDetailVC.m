//
//  BFDetailVC.m
//  BalloonFiesta
//
//  Created by Matthew Ferguson on 1/18/16.
//

#import "BFDetailVC.h"

@interface BFDetailVC ()

@end

@implementation BFDetailVC


@synthesize singleBalloon;

@synthesize backgroundView;
@synthesize balloonView;

@synthesize balloonNameLabel;
@synthesize balloonBannerNumberLabel;
@synthesize balloonLaunchLocationLabel;
@synthesize balloonPilotNameLabel;
@synthesize balloonCityLabel;
@synthesize balloonHomeCountryLabel;
@synthesize balloonManufacturerYrLabel;

@synthesize buttonDone;


-(void)viewDidLoad
{
    [super viewDidLoad];
}



-(void)viewWillAppear:(BOOL)animated
{
    
    UIFont *font = [UIFont fontWithName:@"HelveticaNeue-Light" size:22.0f];
    NSMutableDictionary *navBarTextAttributes =
    [NSMutableDictionary dictionaryWithCapacity:1];
    [navBarTextAttributes setObject:font forKey:NSFontAttributeName];
    self.navigationController.navigationBar.titleTextAttributes = navBarTextAttributes;
    
    UIImage * tempImage;
    tempImage = [UIImage imageNamed:[NSString stringWithFormat:@"%@.png", self.singleBalloon.myPicture]];
    if(tempImage)
    {
        // good to go.
    }
    else{
        tempImage = [UIImage imageNamed:[NSString stringWithFormat:@"%@", @"not_available.png"]];
    }
    
    self.balloonView.image = tempImage;
    self.balloonNameLabel.text = self.singleBalloon.balloonName;
    self.balloonBannerNumberLabel.text = [NSString stringWithFormat:@"Banner Number: %@", self.singleBalloon.aibfNumberString];
    self.balloonLaunchLocationLabel.text = [NSString stringWithFormat:@"Launch Location: %@", self.singleBalloon.launchLocation];
    self.balloonPilotNameLabel.text =  [NSString stringWithFormat:@"%@", self.singleBalloon.pilot];
    self.balloonCityLabel.text =  [NSString stringWithFormat:@"Location: %@",self.singleBalloon.cityNstate];
    self.balloonHomeCountryLabel.text = [NSString stringWithFormat:@"Country: %@",self.singleBalloon.country];
    self.balloonManufacturerYrLabel.text = [NSString stringWithFormat:@"Manufactured: %@", self.singleBalloon.manufactured];
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


#pragma mark - Navigation


-(IBAction) DoneAction:(id)sender
{
    self.backgroundScrollView.backgroundColor = [UIColor cyanColor];
}

// In a storyboard-based application, you will often want to do a
//    little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}


@end
