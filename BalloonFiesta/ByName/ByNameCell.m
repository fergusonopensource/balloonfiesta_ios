//
//  ByNameCell.m
//  BalloonFiesta
//
//  Created by Matthew Ferguson on 1/16/16.
//

#import "ByNameCell.h"

@implementation ByNameCell

@synthesize ByNameTextLabel, ByNameThumbnail, singleBalloon;


- (void)awakeFromNib
{
    self.ByNameTextLabel.text = [[NSString alloc]init];
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}


// The cell node setter
// We implement this because the table cell values need
// to be updated when this property changes, and this allows
// for the changes to be encapsulated
- (void)setBalloon:(SingleBalloon *)anElement
{
    
    self.ByNameTextLabel.text = self.singleBalloon.balloonName;
    
    UIImage * tempImage;
    tempImage = [UIImage imageNamed:self.singleBalloon.myPicture];
    if(tempImage)
    {
        // good to go. 
    }
    else
    {
        tempImage = [UIImage imageNamed:@"not_available.png"];
    }
 
    self.ByNameThumbnail.image = tempImage;
}







@end
